﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public AudioSource[] tracks;
	public AudioSource deathSound;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ActivateDeathSound (){
		for (int i = 0; i < tracks.Length; i++) {
			tracks [i].mute = true;
			deathSound.Play ();
		}
	}
	public void ActivateTrack(int trackNumber) {
		tracks [trackNumber].mute = false;
	}
	public void ActivateDrum(){
		tracks [5].mute=false;
	}

}
