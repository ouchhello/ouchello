﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trappolaScript : MonoBehaviour {
	public GameObject[] trappolas;
	public GameObject[] carillons;
	// Use this for initialization
	void Start () {

		string[] strings = new string[2];

		strings[0] = "first string";
		strings[1] = "second string";
			
		int i = 0;
		foreach (GameObject carillon in GameObject.FindGameObjectsWithTag ("Carillon")) {
			carillons [i] = carillon;
			carillon.SetActive (false);
			i = i + 1;
		}

	}
	
	// Update is called once per frame
	void Update () {
		

	}
	void OnTriggerStay(Collider other){
		if (other.tag == "Player") {
			if (Input.GetKeyDown (KeyCode.E)) {
				print ("aziona trappole");
				foreach (GameObject trappola in trappolas)  
					trappola.GetComponent<Animator> ().CrossFade ("trappola ingranaggio", 0);
				
				foreach (GameObject carillon in carillons) {
					if (carillon != null)carillon.SetActive (true);
				}
			}
		}
	}
}
