﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InizializzaLivello : MonoBehaviour {

	public GameObject [] characters;

	void Start () {
		if (PlayerPrefs.HasKey ("Personaggio")) {
			
			if (PlayerPrefs.GetString ("Personaggio") == "Gravitus") {
				characters [1].SetActive (true);
			} else if (PlayerPrefs.GetString ("Personaggio") == "G.P.") {
				characters [2].SetActive (true);
			} else if (PlayerPrefs.GetString ("Personaggio") == "Skifo") {
				characters [3].SetActive (true);
			} else if (PlayerPrefs.GetString ("Personaggio") == "Denki Ushi") {
				characters [4].SetActive (true);
			} else {
				//hatboy come default
				characters [0].SetActive (true);
			}
		}
	}
	// Update is called once per frame

			void Update () {
	}
}
