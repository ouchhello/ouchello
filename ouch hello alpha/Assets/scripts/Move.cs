﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Move : MonoBehaviour {

	public float speed = 1.0f;
	public int score=0;
	public Text scoreUI;
	public Animator bossAnimator;
	public int rotationSpeed;
	public AudioSource sorgenteSonoraPiedi;
	public float jumpForce;
	public bool atterrato;

	void Update() {
	// muoversi avanti e indietro
		float inputAvantiIndietro = Input.GetAxis("Vertical"); // 0 se non premuto 1 se avanti -1 se indietro
		float indipendenzaDallePiattaforme=Time.deltaTime; 
		Vector3 avantiSecondoMe = transform.forward;
		Vector3 posizionePrecedente = transform.position;
		transform.position = posizionePrecedente + indipendenzaDallePiattaforme * speed * inputAvantiIndietro * avantiSecondoMe;
 	//ruotare (vecchio)
		//Vector3 rotazionePrecedente = transform.eulerAngles;
		//float nuovaRotazioneY = rotazionePrecedente.y + rotationSpeed*Input.GetAxis ("Horizontal")*indipendenzaDallePiattaforme;
		//transform.eulerAngles= new Vector3(rotazionePrecedente.x,nuovaRotazioneY,rotazionePrecedente.z);
	//ruotare (nuovo)
		float rotY =0;
		if (Input.GetKey ("a")){
			rotY -=1;
		}
		if (Input.GetKey ("d")){
			rotY +=1;
		}
		transform.eulerAngles += new Vector3 (0,rotY,0)* 200 * Time.deltaTime; 
	//sprint
		if ((Input.GetKeyDown (KeyCode.LeftShift))||(Input.GetKeyDown (KeyCode.RightShift))) {
			speed += 10;
		}
		if (Input.GetKeyUp ((KeyCode.LeftShift))||(Input.GetKeyDown (KeyCode.RightShift))) {
			speed -= 10; print ("ko");}


		if ((Input.GetKeyDown (KeyCode.W))|| (Input.GetKeyDown (KeyCode.S))) {
			GetComponent<Animator> ().CrossFade ("walk", 0.1f);

		}
			

		if ((Input.GetKeyUp (KeyCode.W))|| (Input.GetKeyUp (KeyCode.S))) {
			GetComponent<Animator> ().CrossFade ("idle", 0.1f);

		}
		Debug.DrawRay (transform.position+Vector3.up*0.5f, Vector3.down*1 );
		RaycastHit hit;
		atterrato=Physics.Raycast (transform.position+Vector3.up*0.5f, Vector3.down,out hit,1);
		if ( atterrato) { // se ho sotto i piedi qualcosa

				// -------- salto ----------------------------
				if (Input.GetKeyDown (KeyCode.Space)) {
				GetComponent<Animator> ().CrossFade ("jump", 0.1f);
					GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
				}	
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag =="speedup") {
			speed = speed + 1;
		}
		if (other.tag == "Carillon") {
			Animator carillonAnimator=other.GetComponentInChildren<Animator>();
			carillonAnimator.CrossFade ("giraruota", 0);
			score = score + 1;
			scoreUI.text = "punteggio:" + score;
			if (score == 10) {
				bossAnimator.CrossFade("apri_botola",0);
			}
		}

	}

	void OnTriggerExit(Collider other){
		if (other.tag =="speedup"){
		speed = speed - 1;
		}
	}

	public void SuonoDelPasso(){
		float numeroACaso = Random.Range (1000, 5000)/1000f;
		//string coseDaStampare = "il numero è: " + numeroACaso;
		//print (coseDaStampare);
		sorgenteSonoraPiedi.pitch = numeroACaso;
		sorgenteSonoraPiedi.Play ();
	}
}