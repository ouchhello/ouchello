using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveAlex : MonoBehaviour {

	public float speed = 1.0f;
	public int score=0;
	public Text scoreUI;
	public Animator botolaAnimator;
	public int rotationSpeed;
//	public AudioSource sorgenteSonoraPiedi;
	public float jumpForce;
	public float superJumpForce;
	public bool atterrato;
	public bool atterratoNelFramePrecedente;
	public float walkSpeed;
	public float runSpeed;
	public bool gameover=false;
	public bool win=false;
	public GameObject gameoverpanel;
	public GameObject winpanel;
	public int carillonNumber;

	void Awake(){
		carillonNumber= GameObject.FindGameObjectsWithTag ("Carillon").Length;
	}

	void Update() {
//		print (GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0)..ToString());
		if ((gameover == false) && (!GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("grab"))) {
			if (gameoverpanel.activeSelf) {
				GetComponent <Animator> ().CrossFade ("Death", 0);
				gameover = true;
			}
			if (winpanel.activeSelf) {
				gameover = true;
			}
		}
	
			atterratoNelFramePrecedente = atterrato;
			Debug.DrawRay (transform.position + Vector3.up * 0.5f, Vector3.down * 1);
			RaycastHit hit;
			atterrato = Physics.Raycast (transform.position + Vector3.up * 0.5f, Vector3.down, out hit, 1);
			// muoversi avanti e indietro
			float inputAvantiIndietro = Input.GetAxis ("Vertical"); // 0 se non premuto 1 se avanti -1 se indietro
			float indipendenzaDallePiattaforme = Time.deltaTime; 
			Vector3 avantiSecondoMe = transform.forward;
			Vector3 posizionePrecedente = transform.position;
			transform.position = posizionePrecedente + indipendenzaDallePiattaforme * speed * inputAvantiIndietro * avantiSecondoMe;
			//ruotare (vecchio)
			//Vector3 rotazionePrecedente = transform.eulerAngles;
			//float nuovaRotazioneY = rotazionePrecedente.y + rotationSpeed*Input.GetAxis ("Horizontal")*indipendenzaDallePiattaforme;
			//transform.eulerAngles= new Vector3(rotazionePrecedente.x,nuovaRotazioneY,rotazionePrecedente.z);
			//ruotare (nuovo)
			float rotY = 0;
			if (Input.GetKey ("a")) {
				rotY -= 1;
			}
			if (Input.GetKey ("d")) {
				rotY += 1;
			}
			transform.eulerAngles += new Vector3 (0, rotY, 0) * 200 * Time.deltaTime; 
			if (!atterratoNelFramePrecedente && atterrato) { //se appena atterrato
				if ((Input.GetKey (KeyCode.W)) || (Input.GetKey (KeyCode.S))) { //se premuti avanti o indietro
					if ((Input.GetKey (KeyCode.LeftShift)) || (Input.GetKey (KeyCode.RightShift))) { //se corre
						GetComponent<Animator> ().CrossFade ("Robot2_run", 0);
					} else { // se cammina
						GetComponent<Animator> ().CrossFade ("walk_alex", 0);
					}
				}
			}
			if ((Input.GetKeyDown (KeyCode.LeftShift))||(Input.GetKeyDown (KeyCode.RightShift))) {
				speed = runSpeed;
				if (atterrato) {
					GetComponent<Animator> ().CrossFade ("Robot2_run", 0);}
			}
			if ((Input.GetKeyUp (KeyCode.LeftShift))||(Input.GetKeyUp (KeyCode.RightShift))) {
				speed = walkSpeed;
				if (atterrato) {
					GetComponent<Animator> ().CrossFade ("walk_alex", 0);}
			}
			if ((Input.GetKeyDown (KeyCode.W))|| (Input.GetKeyDown (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("walk_alex", 0.1f);
			}
				
			if ((Input.GetKeyUp (KeyCode.W))|| (Input.GetKeyUp (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("idle", 0.1f);
			}
			if (atterrato) { // se ho sotto i piedi qualcosa

				// -------- salto ----------------------------
				if (Input.GetKeyDown (KeyCode.Space)) {

					GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
					GetComponent<Animator> ().CrossFade ("Jump_animation", 0);
				}
				//---------------superSalto---------------------------------
				if (Input.GetKeyDown (KeyCode.M)) {
					GetComponent<Rigidbody> ().AddForce (Vector3.up * superJumpForce);
					GetComponent<Animator> ().CrossFade ("Jump_animation", 0);
				}
			}
		}

	void OnTriggerEnter(Collider other){
		if (other.tag =="speedup"){
			speed = speed + 500;
		}
		if (other.tag == "Carillon") {
			score = score + 1;
			other.GetComponent<Animator> ().CrossFade ("carillon_turn", 0);
			GetComponent<Animator> ().CrossFade ("grab", 0);
		}
		if (score >= carillonNumber) {
			botolaAnimator.CrossFade ("Apri Botola", 0);
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag =="speedup"){
		speed = speed - 500;
		}
	}

//	void OnCollisionEnter (Collision collision) {
//		if (atterrato) {// appena atterrato
//			if (run) {
//				GetComponent<Animator> ().CrossFade ("Robot2_run", 0);
//			} else 
//			{GetComponent<Animator> ().CrossFade ("walk_alex", 0.1f);
//			}
//		}
//	}

//	public void SuonoDelPasso(){
//
//		string coseDaStampare = "il numero è: " + numeroACaso;
//		print (coseDaStampare);
//		sorgenteSonoraPiedi.Play ();
//	}
}