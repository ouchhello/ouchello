﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveLuca : MonoBehaviour {

	public float speed = 1.0f;
	public int score=0;
	public Text scoreUI;
	public Animator bossAnimator;
	public int rotationSpeed;
	public AudioSource sorgenteSonoraPiedi;
	public float jumpForce;
	public bool atterrato;
	public bool atterratoNelFramePrecedente;
	public float walkSpeed;
	public float runSpeed;
	public bool run;
	public GameObject gameoverpanel;
	public GameObject winpanel;
	public bool gameover;
	public int numCarillon;

	void Awake() {
		numCarillon = GameObject.FindGameObjectsWithTag ("Carillon").Length;
	}


	void Update() {
		if (gameover == false) {
			if (gameoverpanel.activeSelf) {
				GetComponent <Animator> ().CrossFade ("Death", 0);
				gameover = true;
			}
			if (winpanel.activeSelf) {
				gameover = true;
			}
			// muoversi avanti e indietro
			float inputAvantiIndietro = Input.GetAxis ("Vertical"); // 0 se non premuto 1 se avanti -1 se indietro
			float indipendenzaDallePiattaforme = Time.deltaTime; 
			Vector3 avantiSecondoMe = transform.forward;
			Vector3 posizionePrecedente = transform.position;
			transform.position = posizionePrecedente + indipendenzaDallePiattaforme * speed * inputAvantiIndietro * avantiSecondoMe;
			//ruotare (vecchio)
			//Vector3 rotazionePrecedente = transform.eulerAngles;
			//float nuovaRotazioneY = rotazionePrecedente.y + rotationSpeed*Input.GetAxis ("Horizontal")*indipendenzaDallePiattaforme;
			//transform.eulerAngles= new Vector3(rotazionePrecedente.x,nuovaRotazioneY,rotazionePrecedente.z);
			//ruotare (nuovo)
			float rotY = 0;
			if (Input.GetKey ("a")) {
				rotY -= 1;
			}
			if (Input.GetKey ("d")) {
				rotY += 1;
			}
			transform.eulerAngles += new Vector3 (0, rotY, 0) * 200 * Time.deltaTime; 
			//sprint
			if ((Input.GetKeyDown (KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
				speed = runSpeed;
				print ("ok");
				if (atterrato)
					GetComponent<Animator> ().CrossFade ("Robot2_sprint", 0);
			}
			if (Input.GetKeyUp ((KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
				speed = walkSpeed;
				print ("ko");
				if (atterrato)
					GetComponent<Animator> ().CrossFade ("walk_alex", 0);
			}

			if (speed == runSpeed) {
				run = true;
			} else {
				run = false;
			}

			Debug.DrawRay (transform.position + Vector3.up * 0.3f, Vector3.down * 1);
			RaycastHit hit;
			atterrato = Physics.Raycast (transform.position + Vector3.up * 0.3f, Vector3.down, out hit, 1);

			if (atterrato) { // se ho sotto i piedi qualcosa

				// -------- salto ----------------------------
				if (Input.GetKeyDown (KeyCode.Space)) {

					GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
					GetComponent<Animator> ().CrossFade ("Jump", 0);
					print ("salta");
			
				}

			}
			atterratoNelFramePrecedente = atterrato;
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag =="speedup") {
			print ("speedup");
			speed = speed + 500;
		}
		if (other.tag == "Carillon") {
			Animator carillonAnimator=other.GetComponentInChildren<Animator>();
			carillonAnimator.CrossFade ("carillon_turn", 0);
			print ("gg");
			score = score + 1;
			scoreUI.text = "punteggio:" + score;
			if (score == numCarillon) {
				bossAnimator.CrossFade("win",0);
				print ("BRAVO"); // Quando vinco distruggerò il boss
			}
		}
	}



	void OnTriggerExit(Collider other){
		if (other.tag =="speedup"){
		print ("slow down");
		speed = speed - 500;
		}
	}
	void OnCollisionEnter (Collision collision) {
		if (atterrato) {// appena atterrato
			if (run) {
				GetComponent<Animator> ().CrossFade ("Robot2_sprint", 0);
			} else 
			{GetComponent<Animator> ().CrossFade ("walk_alex", 0);
			}
		}
	}
		
	public void SuonoDelPasso(){
		float numeroACaso = Random.Range (1000, 5000)/1000f;

		sorgenteSonoraPiedi.pitch = numeroACaso;
		sorgenteSonoraPiedi.Play ();
	}
}