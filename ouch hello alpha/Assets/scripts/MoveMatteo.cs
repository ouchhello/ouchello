﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveMatteo : MonoBehaviour {

	public float speed = 1.0f;
	public int score=0;
	public Text scoreUI;
	public Animator bossAnimator;
	public int rotationSpeed;
	public AudioSource sorgenteSonoraPiedi;
	public float jumpForce;
	public bool atterrato;
	public bool isGameOver=false;
	public GameObject gameoverpanel;
	public int numCarillon;
	public bool grabbing;
	public float startgrabtime;
	void Awake (){
		numCarillon = GameObject.FindGameObjectsWithTag ("Carillon").Length;
	}

	void Start () {
		grabbing = false;
		}

	void Update() {
		if (!isGameOver && !grabbing) {
			if (gameoverpanel.activeSelf) {
				isGameOver = true;
			}
			// muoversi avanti e indietro
			float inputAvantiIndietro = Input.GetAxis ("Vertical"); // 0 se non premuto 1 se avanti -1 se indietro
			float indipendenzaDallePiattaforme = Time.deltaTime; 
			Vector3 avantiSecondoMe = transform.forward;
			Vector3 posizionePrecedente = transform.position;
			transform.position = posizionePrecedente + indipendenzaDallePiattaforme * speed * inputAvantiIndietro * avantiSecondoMe;
			//ruotare (vecchio)
			//Vector3 rotazionePrecedente = transform.eulerAngles;
			//float nuovaRotazioneY = rotazionePrecedente.y + rotationSpeed*Input.GetAxis ("Horizontal")*indipendenzaDallePiattaforme;
			//transform.eulerAngles= new Vector3(rotazionePrecedente.x,nuovaRotazioneY,rotazionePrecedente.z);
			//ruotare (nuovo)
			float rotY = 0;
			if (Input.GetKey ("a")) {
				rotY -= 1;
			}
			if (Input.GetKey ("d")) {
				rotY += 1;
			}
			transform.eulerAngles += new Vector3 (0, rotY, 0) * 200 * Time.deltaTime; 
			//sprint
			if ((Input.GetKeyDown (KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
				speed += 10;
			}
			if (Input.GetKeyUp ((KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
				speed -= 10;
				print ("ko");
			}


			if ((Input.GetKeyDown (KeyCode.W)) || (Input.GetKeyDown (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("walk", 0.1f);

			}
				

			if ((Input.GetKeyUp (KeyCode.W)) || (Input.GetKeyUp (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("idle", 0.1f);

			}
			Debug.DrawRay (transform.position + Vector3.up * 0.5f, Vector3.down * 1);
			RaycastHit hit;
			atterrato = Physics.Raycast (transform.position + Vector3.up * 0.5f, Vector3.down, out hit, 1);
			if (atterrato) { // se ho sotto i piedi qualcosa

				// -------- salto ----------------------------
				if (Input.GetKeyDown (KeyCode.Space)) {
					GetComponent<Animator> ().CrossFade ("jump", 0.1f);
					GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
				}
			}	

		} else {
			if (Time.time > startgrabtime + 0.5f) {
				endGrabbing ();
			}
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag =="speedup") {
			speed = speed + 1;
		}
		if (other.tag == "Carillon") {
			other.isTrigger = false;
			Animator carillonAnimator=other.GetComponentInChildren<Animator>();
			GetComponent <Animator> ().CrossFade ("grab", 0.1f);
			grabbing = true;
			startgrabtime = Time.time;
			carillonAnimator.CrossFade ("carillon_turn", 0);
			score = score + 1;
			if (score == numCarillon) {
				print ("Presi tutti");
				bossAnimator.CrossFade("Apri Botola",0);
			}
		}

	}

	public void endGrabbing (){
		grabbing = false;
	}

	void OnTriggerExit(Collider other){
		if (other.tag =="speedup"){
		speed = speed - 1;
		}
	}

	public void SuonoDelPasso(){
		float numeroACaso = Random.Range (1000, 5000)/1000f;
		//string coseDaStampare = "il numero è: " + numeroACaso;
		//print (coseDaStampare);
		sorgenteSonoraPiedi.pitch = numeroACaso;
		sorgenteSonoraPiedi.Play ();
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag != "Slippery") {
			gameObject.GetComponent <Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;

			transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0);
		}
	}

}