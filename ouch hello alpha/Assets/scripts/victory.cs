﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class victory : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

//	public void OnNextPressed(){
//		SceneManager.LoadScene ();
//	}

	public void OnRetryPressed(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void OnMainMenuPressed(){
		SceneManager.LoadScene ("MainUI");
	}

	public void OnNextLevelPressed(){

		if (PlayerPrefs.HasKey ("Livello")) {

			if (PlayerPrefs.GetString ("Livello") == "LevelAnna") {
				PlayerPrefs.SetString ("Livello", "LevelMichael");
				SceneManager.LoadScene ("LevelMichael");
			} else if (PlayerPrefs.GetString ("Livello") == "LevelMichael") {
				PlayerPrefs.SetString ("Livello", "LevelAlex");
				SceneManager.LoadScene ("LevelAlex");
			} else if (PlayerPrefs.GetString ("Livello") == "LevelAlex") {
				PlayerPrefs.SetString ("Livello", "LevelLuca");
				SceneManager.LoadScene ("LevelLuca");
			} else if (PlayerPrefs.GetString ("Livello") == "LevelLuca") {
				PlayerPrefs.SetString ("Livello", "LevelMatteo");
				SceneManager.LoadScene ("LevelMatteo");
			} else {
				//default
				SceneManager.LoadScene ("MainUI");
			}
		}

		}



}
