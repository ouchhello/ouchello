﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSound : MonoBehaviour {
	public int carillonIndex;
	SoundManager soundManager;
	// Use this for initialization
	void Start () {
		soundManager = GameObject.FindObjectOfType<SoundManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			soundManager.ActivateTrack (carillonIndex);
		} 

	}
}
