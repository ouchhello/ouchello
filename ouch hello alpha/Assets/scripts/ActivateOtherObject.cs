﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOtherObject : MonoBehaviour {
	public GameObject otherObjectToActivate;
	public float timeToActivate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(){
		otherObjectToActivate.GetComponent<Animator> ().CrossFade ("on", timeToActivate);
	}
}
