using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveAnna : MonoBehaviour {

	public float speed = 1.0f;
	public int score=0;
	public Text scoreUI;
	public Animator bossAnimator;
	public int rotationSpeed;
	public AudioSource sorgenteSonoraPiedi;
	public float jumpForce;
	public bool atterrato;
	public bool atterratoNelFramePrecedente;
	public float walkSpeed;
	public float runSpeed;
	public bool run;
	public int numCarillon;
	public bool gameover=false;
	public bool win=false;
	public GameObject gameoverpanel;
	public GameObject winpanel;



	void Awake() {
		numCarillon = GameObject.FindGameObjectsWithTag ("Carillon").Length;
	}


	void Update() {
		
		// ANIMAZIONE MORTE
		if (gameover == false) {
			if (gameoverpanel.activeSelf) {
				GetComponent <Animator> ().CrossFade ("Death_ossa", 0);
				gameover = true;
			}
			if (winpanel.activeSelf) {
				gameover = true;
			}
			//verifica se atterra
			Debug.DrawRay (transform.position + Vector3.up * 0.3f, Vector3.down * 1);
			RaycastHit hit;
			atterrato = Physics.Raycast (transform.position + Vector3.up * 0.3f, Vector3.down, out hit, 1);
			atterratoNelFramePrecedente = atterrato;
		
			// muoversi avanti e indietro
			float inputAvantiIndietro = Input.GetAxis ("Vertical"); // 0 se non premuto 1 se avanti -1 se indietro
			float indipendenzaDallePiattaforme = Time.deltaTime; 
			Vector3 avantiSecondoMe = transform.forward;
			Vector3 posizionePrecedente = transform.position;
			transform.position = posizionePrecedente + indipendenzaDallePiattaforme * speed * inputAvantiIndietro * avantiSecondoMe;

			//ruotare (nuovo)
			float rotY = 0;
			if (Input.GetKey ("a")) {
				rotY -= 1;
			}
			if (Input.GetKey ("d")) {
				rotY += 1;
			}
			transform.eulerAngles += new Vector3 (0, rotY, 0) * 200 * Time.deltaTime;

//			//sprint
//			if ((Input.GetKeyDown (KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
//				speed = runSpeed;
//				print ("sprinting");
//				if (atterrato)
//					GetComponent<Animator> ().CrossFade ("Robot2_run", 0);
//			}
//			if ((Input.GetKeyUp (KeyCode.LeftShift)) || (Input.GetKeyDown (KeyCode.RightShift))) {
//				speed = walkSpeed;
//				print ("stopsprint");
//				if (atterrato)
//					GetComponent<Animator> ().CrossFade ("walk_alex", 0);
//			}
//
//			if (speed == runSpeed) {
//				run = true;
//			} else {
//				run = false;
//			}
//
//
//			// -------- salto ----------------------------

//
//			if (atterrato) { // se ho sotto i piedi qualcosa
//
//				if (Input.GetKeyDown (KeyCode.Space)) {
//
//					GetComponent<Rigidbody> ().velocity = (Vector3.up * jumpForce);
//					GetComponent<Animator> ().CrossFade ("Jump_animation", 0);
//					print ("salta");
//				}
//			}
			if (!atterratoNelFramePrecedente && atterrato) { //se appena atterrato
				if ((Input.GetKey (KeyCode.W)) || (Input.GetKey (KeyCode.S))) { //se premuti avanti o indietro
					if ((Input.GetKey (KeyCode.LeftShift)) || (Input.GetKey (KeyCode.RightShift))) { //se corre
						GetComponent<Animator> ().CrossFade ("Robot2_run", 0);
					} else { // se cammina
						GetComponent<Animator> ().CrossFade ("walk_alex", 0);
					}
				}
			}
			if ((Input.GetKeyDown (KeyCode.LeftShift))||(Input.GetKeyDown (KeyCode.RightShift))) {
				speed = runSpeed;
				if (atterrato) {
					GetComponent<Animator> ().CrossFade ("Robot2_run", 0);}
			}
			if ((Input.GetKeyUp (KeyCode.LeftShift))||(Input.GetKeyUp (KeyCode.RightShift))) {
				speed = walkSpeed;
				if (atterrato) {
					GetComponent<Animator> ().CrossFade ("walk_alex", 0);}
			}
			if ((Input.GetKeyDown (KeyCode.W))|| (Input.GetKeyDown (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("walk_alex", 0.1f);
			}

			if ((Input.GetKeyUp (KeyCode.W))|| (Input.GetKeyUp (KeyCode.S))) {
				GetComponent<Animator> ().CrossFade ("idle", 0.1f);
			}



			if (atterrato) { // se ho sotto i piedi qualcosa

				// -------- salto ----------------------------
				if (Input.GetKeyDown (KeyCode.Space)) {

					GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
					GetComponent<Animator> ().CrossFade ("Jump_animation", 0);
				}
				//---------------superSalto---------------------------------
//				if (Input.GetKeyDown (KeyCode.M)) {
//					GetComponent<Rigidbody> ().AddForce (Vector3.up * superJumpForce);
//					GetComponent<Animator> ().CrossFade ("Jump_animation", 0);
//				}

			}

		
		}
	}

	void OnTriggerEnter(Collider other){
		//pedana per accellerare
		if (other.tag == "speedup") {
		print ("speedup");
		speed = speed + 100;
		}

		if (other.tag == "Carillon") {
			print ("carillon get");
			Animator carillonAnimator = other.GetComponentInChildren<Animator> ();
			carillonAnimator.CrossFade ("carillon_turn", 0);
			carillonAnimator.gameObject.GetComponent<Collider> ().isTrigger = false;
			score = score + 1;
			scoreUI.text = "Punteggio: " + score;
			if (score >= numCarillon) {
				bossAnimator.CrossFade ("Apri Botola", 0);
				print ("Un passaggio si è aperto!");
			}
		}
	}

	void OnCollisionEnter (Collision collision) {
		if (atterrato) {// appena atterrato
			if (run) {
				GetComponent<Animator> ().CrossFade ("Robot2_run", 0);
			} else 	{
				GetComponent<Animator> ().CrossFade ("walk_alex", 0);
			}
		}
	}

	//pedana per rallentare
	void OnTriggerExit(Collider other){
		if (other.tag =="speedup"){
		print ("slow down");
		speed = speed - 500;
		}
	}

		
	public void SuonoDelPasso(){
		float numeroACaso = Random.Range (1000, 5000)/1000f;

		sorgenteSonoraPiedi.pitch = numeroACaso;
		sorgenteSonoraPiedi.Play ();
	}
}